##############
# Constantes #
##############

# Repertoires
SOURCE = ./src
BUILD = ./build
APPS = ./apps
BIN = ./bin

DOCPATH = ${SOURCE}/dox
DOCTARGET = ./doc
DIRLIST = ${SOURCE} ${BIN} ${BUILD} ${DOCTARGET}

# Commandes
CC = gcc

# Options
CFLAGS = -O0 -g -W -Wall -Wextra -Wconversion -Werror -mtune=native  -march=native  -std=c11 -D_DEFAULT_SOURCE 
LDFLAGS = -lm -W -Wall -pedantic -L. -lm  -pthread

# Fichiers
DOX = ${wildcard ${DOCPATH}/*.dox} # Sources
SRC = ${wildcard ${SOURCE}/*.c} # Sources
TGTS = ${wildcard ${APPS}/*.c} # Sources
INT = ${wildcard ${SOURCE}/*.h} # Interfaces
OBJ = ${SRC:${SOURCE}/%.c=${BUILD}/%.o}

# Cibles
BINTGTS = ${TGTS:${APPS}/%.c=${BIN}/%}

##########
# Regles #
##########

# ALL
.PHONY : all
all : dirs doc ${BINTGTS} 

# CLEAN
.PHONY : clean
clean :
	@echo
	@echo Cleaning : object files
	@echo --------
	@echo
	rm -f ${OBJ}
	@echo
	@echo Done
	@echo

.PHONY : clean-doc
clean-doc :
	@echo
	@echo Cleaning : doc files
	@echo --------
	@echo
	rm -fr ${DOCTARGET}
	@echo
	@echo Done
	@echo

.PHONY : clean-emacs
clean-emacs :
	@echo
	@echo Cleaning : emacs back-ups
	@echo --------
	@echo
	rm -f ${SOURCE}/*~
	rm -f ${SOURCE}/\#*\#
	rm -f *~
	rm -f \#*\#
	@echo
	@echo Done
	@echo
	
.PHONY : clean-bin
clean-bin :
	@echo
	@echo Cleaning : binaries
	@echo --------
	@echo
	rm -f ${BINTGTS} 
	@echo
	@echo Done
	@echo
	
.PHONY : distclean
distclean : clean clean-emacs clean-bin clean-doc
	@echo
	@echo All done
	@echo

.PHONY : dirs
dirs : 
	@echo
	@echo Build needes directories
	@echo ------------------ 
	@for dir in ${DIRLIST} ;\
	do \
	    echo -n Creating directory $${dir}": ";\
	    if test -d $${dir} ;\
	    then \
		echo Directory already exists ;\
	    else mkdir -p $${dir} ;\
	    fi ;\
	done; \
    echo done


# Binaires
${BIN}/% : ${BUILD}/%.o ${OBJ}
	@echo
	@echo Linking bytecode : $@
	@echo ----------------
	${CC} -o $@ $^ ${LDFLAGS} -I${SOURCE}
	@echo
	@echo Done
	@echo

# Regles generiques
${BUILD}/%.o : ${SOURCE}/%.c ${SOURCE}/%.h
	@echo
	@echo Compiling $@
	@echo --------
	@echo 
	$(CC) $(CFLAGS) -c $< -o $@ -I${SOURCE}
	@echo Done
	@echo

# Regles generiques
${BUILD}/%.o : ${APPS}/%.c 
	@echo
	@echo Compiling $@
	@echo --------
	@echo
	$(CC) $(CFLAGS) -c $< -o $@ -I${SOURCE}
	@echo
	@echo Done
	@echo

# Documentation 
.PHONY : doc
doc : ${SRC} ${INT} ${DOX}
	@echo
	@echo Build documentation
	@echo --------
	@echo
	@doxygen
	@echo Done
	@echo

#############################
# Inclusion et spécificités #
#############################


